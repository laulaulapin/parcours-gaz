/* -----------------------------*/
/* FUNCTIONS TO DISPLAY ALERTS */
/* -----------------------------*/
function MessageAlert(type, message) {
    let id = uniqId();
    let content = "<div id='" + id + "' class='alert alert-" + type + "' role='alert' style='display: none'>"
        + "<button type='button' class='close'>"
        + "<span aria-hidden='true' class='glyphicon glyphicon-remove'></span>"
        + "</button><p>"
        + message
        + "</p></div>";
    $('#spaceAlert').prepend(content);

    let alert = $('#spaceAlert').find("#" + id);
    alert.fadeIn(500).delay(10000).fadeOut(500);

    let close = $('#spaceAlert').find("#" + id).find(".close");
    close.click(function () {
        alert.stop().fadeOut(500);
    })
}


/* -----------------------------*/
/* FUNCTIONS TO MANAGE STYLE OF FIELDS FORMS */
/* -----------------------------*/
// function to check content of fields
// -> we need groupInput to display pictogramme,
// -> input to add style of border
// -> and regEx to check the content
function testStringInput(type, groupInput, input, regEx, min, max) {
    let str = input.value;

    if (type == "text") {
        let lengthInput = str.length;
        let tableau_correspondances = str.match(regEx);

        if (!(tableau_correspondances == 0) && !(tableau_correspondances === null)) {
            let long_tab = tableau_correspondances.length;
            let long_str = str.length;

            if ((!(long_str == long_tab)) || (lengthInput < min || lengthInput > max)) {
                input.setAttribute("style", "border-color : #b51d45");
                input.classList.add('fail');
                groupInput.getElementsByClassName("glyphicon-ok")[0].style.display = "none";
                groupInput.getElementsByClassName("glyphicon-remove")[0].style.display = "block";
                groupInput.setAttribute("style", "color : #b51d45");
            }
            else {
                input.setAttribute("style", "border-color : #1bd57b");
                input.classList.remove('fail');
                groupInput.getElementsByClassName("glyphicon-remove")[0].style.display = "none";
                groupInput.getElementsByClassName("glyphicon-ok")[0].style.display = "block";
                groupInput.setAttribute("style", "color : #1bd57b");
            }
        }

    } else if (type == "email")
    {
        let testEmail = regEx.test(input.value);
        if (testEmail === true) {
            input.setAttribute("style", "border-color : #1bd57b");
            input.classList.remove('fail');
            groupInput.getElementsByClassName("glyphicon-remove")[0].style.display = "none";
            groupInput.getElementsByClassName("glyphicon-ok")[0].style.display = "block";
            groupInput.setAttribute("style", "color : #1bd57b");
        }
        else {
            input.setAttribute("style", "border-color : #b51d45");
            input.classList.add('fail');
            groupInput.getElementsByClassName("glyphicon-ok")[0].style.display = "none";
            groupInput.getElementsByClassName("glyphicon-remove")[0].style.display = "block";
            groupInput.setAttribute("style", "color : #b51d45");
        }
    }
    else {
        input.setAttribute("style", "border-color : #b51d45");
        input.classList.add('fail');
        groupInput.getElementsByClassName("glyphicon-ok")[0].style.display = "none";
        groupInput.getElementsByClassName("glyphicon-remove")[0].style.display = "block";
        groupInput.setAttribute("style", "color : #b51d45");
    }
}

// function to reset style of input, textarea in groupeInput
function resetCssField(groupInput) {
    groupInput.removeAttribute("style");
    groupInput.getElementsByTagName('label')[0].nextElementSibling.removeAttribute("style");
    groupInput.getElementsByClassName("glyphicon")[0].style.display = "none";
    groupInput.getElementsByClassName("glyphicon")[1].style.display = "none";
}


/* -----------------------------*/
/* OTHERS FUNCTIONS */
/* -----------------------------*/
// create an unique number
function uniqId() {
    return Math.round(new Date().getTime() + (Math.random() * 100));
}

/* -----------------------------*/
/* MASONRY INITIALISATION */
/* -----------------------------*/
// Masonry initialisation
$('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: 160
});