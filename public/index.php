<?php
//auto charge toutes nos classes
spl_autoload_register(function($className){
	$className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
	//si le fichier existe dans le sous-dossier app...
	if (file_exists("../app/" . $className . ".php")){
		include("../app/" . $className . ".php");
	}
	//sinon on démarre de la racine
	else {
		include("../" . $className . ".php");
	}
});

//include l'autoload de composer, s'il existe
if (file_exists("../vendor/autoload.php")){
	include("../vendor/autoload.php");
}

//récupère l'URL
$p = (!empty($_GET['p'])) ? $_GET['p'] : "/";

//charge nos routes et notre config
include("../app/routes.php");
include("../app/config.php");

//crée une instance du dispatcher
$dispatcher = new Grill\Controller\Dispatcher();

//fait la correspondance entre l'URL et les routes
$dispatcher->match($routes, $p);
