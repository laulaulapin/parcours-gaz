<?php

namespace Manager;

use Entity\Article;
use Grill\Model\Db;
use Entity\Step;


class StepManager
{


    // function to Create a new step in db
    public function addStepManager(Step $step)
    {

        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("INSERT INTO steps (titleStep, descStep, pictoStep, orderStep, dateCreaStep, dateUpdateStep) VALUES (?,?,?,?,?,?);");
        $stmt->bindValue(1, $step->getTitleStep());
        $stmt->bindValue(2, $step->getDescStep());
        $stmt->bindValue(3, $step->getPictoStep());
        $stmt->bindValue(4, $step->getOrderStep());
        $stmt->bindValue(5, $step->getDateCreaStep());
        $stmt->bindValue(6, $step->getDateUpdateStep());

        $insert = $stmt->execute();

        if ($insert) {
            return true;
        } else {
            return false;
        }

    }

    // function to Update an existing Step in db
    // We need an Object Step : all attributs of Step is updating (not id, id is the condition)
    public function updateStepManager(Step $step)
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("UPDATE steps SET titleStep = ?, descStep = ?, pictoStep = ?, orderStep = ?, dateUpdateStep = ? WHERE idStep = ?;");
        $stmt->bindValue(1, $step->getTitleStep());
        $stmt->bindValue(2, $step->getDescStep());
        $stmt->bindValue(3, $step->getPictoStep());
        $stmt->bindValue(4, $step->getOrderStep());
        $stmt->bindValue(5, $step->getDateUpdateStep());
        $stmt->bindValue(6, $step->getIdStep());

        $update = $stmt->execute();
        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    // function to Delete an existing step in db
    public function deleteStepManager($id)
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("DELETE FROM steps  WHERE idStep = ?;");
        $stmt->bindValue(1, $id);

        $update = $stmt->execute();
        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    // function to select all Steps from db order by orderStep
    public function selectAllSteps()
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("SELECT * FROM steps ORDER BY orderStep");
        $stmt->execute();

        $steps = [];
        while ($step = $stmt->fetchObject('Entity\Step')) {
            //hydrate array of articles and medias
            $idStep = $step->getIdStep();
            $arrayOfArticles = $this->selectArticlesOfStep($idStep);
            $step->setStepArticles($arrayOfArticles);
            $arrayOfMedias = $this->selectMediasOfStep($idStep);
            $step->setStepMedias($arrayOfMedias);

            $steps[] = $step;
        }
        return $steps;
    }

    // function to select one Step from db
    public function selectOneStep($id)
    {
        if (!empty($id)) {
            $pdo = Db::getPdo();
            $stmt = $pdo->prepare("SELECT * FROM steps WHERE idStep = ?");
            $stmt->bindValue(1, $id);
            $stmt->execute();
            $step = $stmt->fetchObject('Entity\Step');

            //hydrate array of articles and medias
            $idStep = $step->getIdStep();
            $arrayOfArticles = $this->selectArticlesOfStep($idStep);
            $step->setStepArticles($arrayOfArticles);
            $arrayOfMedias = $this->selectMediasOfStep($idStep);
            $step->setStepMedias($arrayOfMedias);

            return $step;
        }
        return false;
    }

    // This method return only the orders what is checked
    public function selectAllOrder()
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("SELECT orderStep FROM steps");
        $stmt->execute();

        $orders = [];
        while ($ordersDatas = $stmt->fetch()) {
            foreach ($ordersDatas as $key => $order) {
                if ($key === "orderStep") {
                    $orders[] = $order;
                }
            }
        }
        return $orders;
    }

    // function to select all Articles associatied to Steps
    public function selectArticlesOfStep($idStep)
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("SELECT * FROM articles LEFT JOIN step_has_articles ON article_id = idArticle WHERE step_id = ?");
        $stmt->bindValue(1, $idStep);
        $stmt->execute();

        $articles = [];
        while ($article = $stmt->fetchObject('Entity\Article')) {
            $articles[] = $article;
        }
        return $articles;
    }

    // function to select all Medias associatied to Steps
    public function selectMediasOfStep($idStep)
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("SELECT * FROM medias LEFT JOIN step_has_medias ON media_id = idMedia WHERE step_id = ?");
        $stmt->bindValue(1, $idStep);
        $stmt->execute();

        $medias = [];
        while ($media = $stmt->fetchObject('Entity\Media')) {
            $medias [] = $media;
        }
        return $medias;
    }

    /**
     * return array to serialize objet
     */
    public function jsonSerialize()
    {
        // $_GET['id'] = get one media
        if (isset($_GET['id']) && $_GET['id'] != null) {
            $step = $this->selectOneStep($_GET['id']);
            $arrayStep = [];

            // serialise step for each MEDIA
            $arrayMedias = [];
            $allStepMedias = $step->getStepMedias();
            if ($allStepMedias != 0 && is_array($allStepMedias)) {
                foreach ($allStepMedias as $media) {
                    $arrayMedias[] =
                        [
                            "id" => $media->getIdMedia(),
                            "typemedia" => $media->getTypeMedia(),
                            "titlemedia" => $media->getTitleMedia(),
                            "descmedia" => $media->getDescMedia(),
                            "urlmedia" => $media->getUrlMedia(),
                            "datecreamedia" => $media->getDateCreaMedia(),
                            "dateupdatemedia" => $media->getDateUpdateMedia(),
                            "statutmedia" => $media->getStatutMedia()
                        ];
                }
            }


            // serialise articles for each MEDIA
            $arrayArticles = [];
            $allStepArticles = $step->getStepArticles();
            if ($allStepArticles != 0 && is_array($allStepArticles)) {
                foreach ($allStepArticles as $article) {
                    $arrayArticles[] =
                        [
                            "id" => $article->getIdArticle(),
                            "titlearticle" => $article->getTitleArticle(),
                            "contentarticle" => $article->getContentArticle(),
                            "datecreaarticle" => $article->getDateCreaArticle(),
                            "dateupdatearticle" => $article->getDateUpdateArticle(),
                            "statutarticle" => $article->getStatutArticle()
                        ];
                }
            }

            $arrayStep[] =
                [
                    "id" => $step->getIdStep(),
                    "titlestep" => $step->getTitleStep(),
                    "descstep" => $step->getDescStep(),
                    "pictostep" => $step->getPictoStep(),
                    "orderstep" => $step->getOrderStep(),
                    "steparticles" => $arrayArticles,
                    "stepmedias" => $arrayMedias,
                    "dateUpdatestep" => $step->getDateUpdateStep()
                ];
            return $arrayStep;
        } // else get all medias
        else {

            // steps
            $arraySteps = [];
            $allSteps = $this->selectAllSteps();

            foreach ($allSteps as $step) {

                /// serialise medias for each MEDIA
                $arrayMedias= [];
                $allStepMedias = $step->getStepMedias();
                if ($allStepMedias != 0 && is_array($allStepMedias)) {
                    foreach ($allStepMedias as $media) {
                        $arrayMedias[] =
                            [
                                "id" => $media->getIdMedia(),
                                "typemedia" => $media->getTypeMedia(),
                                "titlemedia" => $media->getTitleMedia(),
                                "descmedia" => $media->getDescMedia(),
                                "urlmedia" => $media->getUrlMedia(),
                                "datecreamedia" => $media->getDateCreaMedia(),
                                "dateupdatemedia" => $media->getDateUpdateMedia(),
                                "statutmedia" => $media->getStatutMedia()
                            ];
                    }
                }


                // serialise articles for each MEDIA
                $arrayArticles = [];
                $allStepArticles = $step->getStepArticles();
                if ($allStepArticles != 0 && is_array($allStepArticles)) {
                    foreach ($allStepArticles as $article) {
                        $arrayArticles[] =
                            [
                                "id" => $article->getIdArticle(),
                                "titlearticle" => $article->getTitleArticle(),
                                "contentarticle" => $article->getContentArticle(),
                                "datecreaarticle" => $article->getDateCreaArticle(),
                                "dateupdatearticle" => $article->getDateUpdateArticle(),
                                "statutarticle" => $article->getStatutArticle()
                            ];
                    }
                }

                $arraySteps[] =
                    [
                        "id" => $step->getIdStep(),
                        "titlestep" => $step->getTitleStep(),
                        "descstep" => $step->getDescStep(),
                        "pictostep" => $step->getPictoStep(),
                        "orderstep" => $step->getOrderStep(),
                        "steparticles" => $arrayArticles,
                        "stepmedias" => $arrayMedias,
                        "dateUpdatestep" => $step->getDateUpdateStep()
                    ];
            }
            return $arraySteps;
        }
    }
}




