<?php

namespace Manager;

use Entity\Article;
use Entity\Step;
use Grill\Model\Db;


class ArticleManager implements \JsonSerializable
{


    // function to Create a new Article in db
    // we test if there is already content because it must be one only main in DB
    public function addArticleManager(Article $article)
    {

        // we test if main already exist in bdd because it must be only one
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("INSERT INTO articles (titleArticle, contentArticle, dateCreaArticle, dateUpdateArticle, statutArticle) VALUES (?,?,?,?,?);");
        $stmt->bindValue(1, $article->getTitleArticle());
        $stmt->bindValue(2, $article->getContentArticle());
        $stmt->bindValue(3, $article->getDateCreaArticle());
        $stmt->bindValue(4, $article->getDateUpdateArticle());
        $stmt->bindValue(5, $article->getStatutArticle());

        $insert = $stmt->execute();

        $idArticle = $pdo->lastInsertId();
        var_dump($article->getArticleSteps());
        var_dump($article);

        if ($insert) {
            // call function to insert line steps/article
            if ($article->getArticleSteps() != 0) {
                $insert2 = $this->addArticlesToStepManager($idArticle, $article->getArticleSteps());
                if ($insert2) {
                    echo $idArticle;
                    return $insert;
                } else {
                    echo $idArticle;
                    return false;
                }
            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    // function to Create a new line of association step/article
    // we test if there is already content because it must be one only main in DB
    public function addArticlesToStepManager($idArticle, $arrayOrSingleIdStep)
    {
        // we test if main already exist in bdd because it must be only one
        $pdo = Db::getPdo();

        if (is_array($arrayOrSingleIdStep)) {
            foreach ($arrayOrSingleIdStep as $singleStep) {
                $stmt = $pdo->prepare("INSERT INTO step_has_articles (step_id, article_id) VALUES (?,?);");
                $stmt->bindValue(1, $singleStep);
                $stmt->bindValue(2, $idArticle);

                $insert = $stmt->execute();

                if ($insert) {
                    continue;
                } else {
                    break;
                }
            }
            return true;


        } else {
            $stmt = $pdo->prepare("INSERT INTO step_has_articles (step_id, article_id) VALUES (?,?);");
            $stmt->bindValue(1, $arrayOrSingleIdStep);
            $stmt->bindValue(2, $idArticle);

            $insert = $stmt->execute();

            if ($insert) {
                return true;
            } else {
                return false;
            }
        }
    }

    // function to Create a new line of association step/article
    // we test if there is already content because it must be one only main in DB
    public function deleteStepsOfArticleManager($idArticle)
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("DELETE FROM step_has_articles WHERE article_id = ?;");
        $stmt->bindValue(1, $idArticle);

        $delete = $stmt->execute();
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }


    // function to Update an existing Article in db
    // We need an Object Article : all attributs of Article is updating (not id, id is the condition)
    public function updateArticleManager(Article $article)
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("UPDATE articles SET titleArticle = ?, contentArticle = ?, dateCreaArticle = ?, dateUpdateArticle = ?, statutArticle = ? WHERE idArticle = ?;");
        $stmt->bindValue(1, $article->getTitleArticle());
        $stmt->bindValue(2, $article->getContentArticle());
        $stmt->bindValue(3, $article->getDateCreaArticle());
        $stmt->bindValue(4, $article->getDateUpdateArticle());
        $stmt->bindValue(5, $article->getStatutArticle());
        $stmt->bindValue(6, $article->getIdArticle());

        $update = $stmt->execute();
        if ($update) {
            // call function to insert line steps/article
            if ($article->getArticleSteps() == 0) {
                $deleteSteps = $this->deleteStepsOfArticleManager($article->getIdArticle());
                if ($deleteSteps) {
                    return true;
                } else {
                    return false;
                }
            } else if ($article->getArticleSteps() != 0) {
                $deleteSteps = $this->deleteStepsOfArticleManager($article->getIdArticle());
                $insertSteps = $this->addArticlesToStepManager($article->getIdArticle(), $article->getArticleSteps());
                if ($deleteSteps && $insertSteps) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // function to Delete an existing Article  in db
    public function deleteArticleManager($id)
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("DELETE FROM articles  WHERE idArticle = ?;");
        $stmt->bindValue(1, $id);

        $delete = $stmt->execute();
        if ($delete) {
            $delete2 = $this->deleteStepsOfArticleManager($id);
            if ($delete2) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // function to select all Steps for each article
    public function selectStepsOfArticle($idArticle)
    {
        $pdo = Db::getPdo();
        //$stmt = $pdo->prepare("SELECT * FROM steps LEFT JOIN step_has_articles ON step_id = idStep AND article_id = ?");
        $stmt = $pdo->prepare("SELECT * FROM steps LEFT JOIN step_has_articles ON step_id = idStep WHERE article_id = ?");
        $stmt->bindValue(1, $idArticle);
        $stmt->execute();

        $steps = [];
        while ($stepDatas = $stmt->fetch()) {
            $step = new Step();
            $step->hydrateSQL($stepDatas);
            $steps[] = $step;
        }
        return $steps;
    }

    // function to select all Articles from db
    public function selectAllArticles()
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("SELECT * FROM articles");
        $stmt->execute();

        $articles = [];
        while ($articleDatas = $stmt->fetch()) {
            $article = new Article();
            $article->hydrateSQL($articleDatas);

            //hydrate array of steps
            $idArticle = $article->getIdArticle();
            $arrayOfSteps = $this->selectStepsOfArticle($idArticle);
            $article->setArticleSteps($arrayOfSteps);

            $articles[] = $article;
        }
        return $articles;
    }

    // function to select one Article from db
    public function selectOneArticle($id)
    {
        if (!empty($id)) {
            $pdo = Db::getPdo();
            $stmt = $pdo->prepare("SELECT * FROM articles WHERE idArticle = ?");
            $stmt->bindValue(1, $id);
            $stmt->execute();
            $articleData = $stmt->fetch();

            $article = new Article();
            $article->hydrateSQL($articleData);

            //hydrate array of steps
            $idArticle = $article->getIdArticle();
            $arrayOfSteps = $this->selectStepsOfArticle($idArticle);
            $article->setArticleSteps($arrayOfSteps);

            return $article;
        }
        return false;
    }

    /**
     * return array to serialize objet
     */
    public function jsonSerialize()
    {
        // articles
        $arrayArticles = [];
        $allArticles = $this->selectAllArticles();

        foreach ($allArticles as $article) {
            // serialise step for each article
            $arraySteps = [];
            $allArticleSteps = $article->getArticleSteps();
            foreach ($allArticleSteps as $step) {
                $arraySteps[] =
                    [
                        "id" => $step->getIdStep(),
                        "titlestep" => $step->getTitleStep(),
                        "descstep" => $step->getDescStep(),
                        "pictostep" => $step->getPictoStep(),
                        "orderstep" => $step->getOrderStep(),
                        "dateUpdatestep" => $step->getDateUpdateStep()
                    ];
            }

            $arrayArticles[] =
                [
                    "id" => $article->getIdArticle(),
                    "titlearticle" => $article->getTitleArticle(),
                    "contentarticle" => $article->getContentArticle(),
                    "articlesteps" => $arraySteps,
                    "datecreaarticle" => $article->getDateCreaArticle(),
                    "dateupdatearticle" => $article->getDateUpdateArticle(),
                    "statutarticle" => $article->getStatutArticle()
                ];
        }
        return $arrayArticles;
    }
}




