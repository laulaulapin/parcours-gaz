<?php

namespace Manager;

use Entity\Step;
use Grill\Model\Db;
use Entity\Main;

class MainManager
{

    // function to Create a new main in db
    // we test if there is already content because it must be one only main in DB
    public function addMainManager(Main $main)
    {
        // we test if main already exist in bdd because it must be only one
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("INSERT INTO main (titleMain, descMain, pictoMain, dateUpdateMain) VALUES (?,?,?,?);");
        $stmt->bindValue(1, $main->getTitleMain());
        $stmt->bindValue(2, $main->getDescMain());
        $stmt->bindValue(3, $main->getPictoMain());
        $stmt->bindValue(4, $main->getDateUpdateMain());

        $insert = $stmt->execute();

        if ($insert) {
            return true;
        } else {
            return false;
        }
    }

    // function to Update an existing user in db
    // We need an Object User : all attributs of User is updating (not id, id is the condition)
    public function updateMainManager(Main $main)
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("UPDATE main SET titleMain = ?, descMain = ?, pictoMain = ?, dateUpdateMain = ?;");
        $stmt->bindValue(1, $main->getTitleMain());
        $stmt->bindValue(2, $main->getDescMain());
        $stmt->bindValue(3, $main->getPictoMain());
        $stmt->bindValue(4, $main->getDateUpdateMain());

        $update = $stmt->execute();
        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    // function to Delete the existing Main in db
    public function deleteMainManager($id)
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("DELETE FROM main WHERE idMain = ?");
        $stmt->bindValue(1, $id);

        $delete = $stmt->execute();
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    // function to select all Main from db
    public function selectAllMain()
    {
        $pdo = Db::getPdo();
        $stmt = $pdo->prepare("SELECT * FROM main");
        $stmt->execute();
        $mainDatas = $stmt->fetch();

        // $userDatas = false if the user not exist
        if ($mainDatas) {
            $main = new Main();
            $main->hydrateSQL($mainDatas);
            $existMain = $main;
        } else {
            $existMain = false;
        }
        return $existMain;
    }


    /**
     * return array to serialize objet
     */
    public function jsonSerialize(Step $step)
    {
        return [
            "id"        =>      $step->getIdStep(),
            "titlemain" =>      $step->getTitleStep(),
            "desc"      =>      $step->getDescStep(),
            "picto"     =>      $step->getPictoStep(),
            "order"     =>      $step->getOrderStep(),
            "dateUpdate" =>     $step->getDateUpdateStep()
        ];
    }

}