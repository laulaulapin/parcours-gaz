<?php

namespace Entity;

class Main implements \JsonSerializable {

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- ATTRIBUTS -------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    private $idMain;                //INT
    private $titleMain;             //VARCHAR(255)
    private $descMain;              //TEXT
    private $pictoMain;             //TEXT
    private $dateUpdateMain;        //DATETIME


    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ---------------------------------------------- SETTERS & GETTERS --------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // $idMain SETTER
    public function setIdMain($id)
    {
        if(intval($id))
        {
            $this->idMain = $id;
        }
    }

    // $idMain GETTER
    public function getIdMain()
    {
        return $this->idMain;
    }

    // $titleMain SETTER
    public function setTitleMain($title)
    {
        if(is_string($title))
        {
            $this->titleMain = $title;
        }
    }

    // $titleMain GETTER
    public function getTitleMain()
    {
        return $this->titleMain;
    }

    // $descMain SETTER
    public function setDescMain($description)
    {
        if(is_string($description))
        {
            $this->descMain = $description;
        }
    }

    // $descMain GETTER
    public function getDescMain()
    {
        return $this->descMain;
    }

    // $pictoMain SETTER
    public function setPictoMain($url)
    {
        if(is_string($url))
        {
            $this->pictoMain = $url;
        }
    }

    // $pictoMain GETTER
    public function getPictoMain()
    {
        return $this->pictoMain;
    }

    // $dateUpdateMain SETTER
    public function setDateUpdateMain($date)
    {
        $this->dateUpdateMain = $date;
    }

    // $dateUpdateMain GETTER
    public function getDateUpdateMain()
    {
        return $this->dateUpdateMain;
    }

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------- METHODS -------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // hydratation of User from datas
    // hydratation of Step from datas
    public function hydrate($datas)
    {
        if(!$datas == "") {
            // We loop all datas (sdatas = array of all attributs of object)
            foreach ($datas as $key => $value) {
                // We create method to call setters with prefix 'set'
                $method = 'set' . ucfirst($key);

                // We check id the method exist
                if (method_exists($this, $method)) {
                    // We call the setter and inject the value
                    $this->$method($value);
                }
            }
        }
    }

    // hydratation of Step from sql
    public function hydrateSQL($arraySql)
    {
        if(!$arraySql == "") {
            // We loop all datas ($arraySql = array of all attributs of object)
            foreach ($arraySql as $key => $value) {
                // We create method to call setters with prefix 'set'
                $method = 'set' . ucfirst($key);

                // We check id the method exist
                if (method_exists($this, $method)) {
                    // We call the setter and inject the value
                    $this->$method($value);
                }
            }
        }
    }


    /**
     * return array to serialize objet
     */
    public function jsonSerialize()
    {
        return [
            "id"        =>      $this->getIdMain(),
            "titlemain" =>      $this->getTitleMain(),
            "desc"      =>      $this->getDescMain(),
            "picto"     =>      $this->getPictoMain(),
            "dateUpdate" =>     $this->getDateUpdateMain()
        ];
    }
}
