<?php

namespace Entity;

class Article  {

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- ATTRIBUTS -------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    private $idArticle;                 //INT
    private $titleArticle;              //VARCHAR(255)
    private $arraySteps;                //ARRAY OF OBJECTS OR OBJECT OR 0 table step_has_articles
    private $contentArticle;            //TEXT
    private $dateCreaArticle;           //TEXT
    private $dateUpdateArticle;         //INT
    private $statutArticle;             //DATETIME

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ---------------------------------------------- SETTERS & GETTERS --------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // $idArticle SETTER
    public function setIdArticle($id)
    {
        if(intval($id))
        {
            $this->idArticle = $id;
        }
    }

    // $idArticle GETTER
    public function getIdArticle()
    {
        return $this->idArticle;
    }

    // $titleArticle SETTER
    public function setTitleArticle($title)
    {
        if(is_string($title))
        {
            $this->titleArticle = $title;
        }
    }

    // $titleArticle GETTER
    public function getTitleArticle()
    {
        return $this->titleArticle;
    }

    // $contentArticle SETTER
    public function setContentArticle($content)
    {
        if(is_string($content))
        {
            $this->contentArticle = $content;
        }
    }

    // $contentArticle GETTER
    public function getContentArticle()
    {
        return $this->contentArticle;
    }

    // $ArticleSteps SETTER
    public function setArticleSteps($arraySteps)
    {
        $this->arraySteps = $arraySteps;
    }

    // $ArticleSteps GETTER
    public function getArticleSteps()
    {
        return $this->arraySteps;
    }

    // $dateCreaArticle SETTER
    public function setDateCreaArticle($dateCreaArticle)
    {
        $this->dateCreaArticle = $dateCreaArticle;
    }

    // $dateCreaArticle GETTER
    public function getDateCreaArticle()
    {
        return $this->dateCreaArticle;
    }

    // $dateUpdateArticle SETTER
    public function setDateUpdateArticle($dateUpdateArticle)
    {
        $this->dateUpdateArticle = $dateUpdateArticle;
    }

    // $dateUpdateArticle GETTER
    public function getDateUpdateArticle()
    {
        return $this->dateUpdateArticle;
    }

    // $statutArticle SETTER
    public function setStatutArticle($statutArticle)
    {
        $this->statutArticle = $statutArticle;
    }

    // $statutArticle GETTER
    public function getStatutArticle()
    {
        return $this->statutArticle;
    }


    /* -------------------------------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------- METHODS -------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // hydratation of Article from datas
    public function hydrate($datas)
    {
        if(!$datas == "") {
            // We loop all datas (sdatas = array of all attributs of object)
            foreach ($datas as $key => $value) {
                // We create method to call setters with prefix 'set'
                $method = 'set' . ucfirst($key);

                // We check id the method exist
                if (method_exists($this, $method)) {
                    // We call the setter and inject the value
                    $this->$method($value);
                }
            }
        }
    }

    // hydratation of Article from sql
    public function hydrateSQL($arraySql)
    {
        if(!$arraySql == "") {
            // We loop all datas ($arraySql = array of all attributs of object)
            foreach ($arraySql as $key => $value) {
                // We create method to call setters with prefix 'set'
                $method = 'set' . ucfirst($key);

                // We check id the method exist
                if (method_exists($this, $method)) {
                    // We call the setter and inject the value
                    $this->$method($value);
                }
            }
        }
    }
}
