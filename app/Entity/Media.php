<?php

namespace Entity;

class Media  {

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- ATTRIBUTS -------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    private $idMedia;                   //INT
    private $typeMedia;                 //VARCHAR(255)
    private $titleMedia;                //VARCHAR(255)
    private $arraySteps;                //ARRAY OF OBJECTS OR OBJECT OR 0 table step_has_Medias
    private $arrayArticles;             //ARRAY OF OBJECTS OR OBJECT OR 0 table article_has_Medias
    private $descMedia;                 //TEXT
    private $urlMedia;                  //INT
    private $dateCreaMedia;           //DATETIME
    private $dateUpdateMedia;           //DATETIME
    private $statutMedia;               //INT
    private $orderMedia;                //INT

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ---------------------------------------------- SETTERS & GETTERS --------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // $idMedia SETTER
    public function setIdMedia($id)
    {
        if(intval($id))
        {
            $this->idMedia = $id;
        }
    }

    // $idMedia GETTER
    public function getIdMedia()
    {
        return $this->idMedia;
    }

    // $titleMedia SETTER
    public function setTypeMedia($type)
    {
        if(is_string($type))
        {
            $this->typeMedia = $type;
        }
    }

    // $titleMedia GETTER
    public function getTypeMedia()
    {
        return $this->typeMedia;
    }

    // $contentMedia SETTER
    public function setTitleMedia($title)
    {
        if(is_string($title))
        {
            $this->titleMedia = $title;
        }
    }

    // $contentMedia GETTER
    public function getTitleMedia()
    {
        return $this->titleMedia;
    }

    // $MediaSteps SETTER
    public function setDescMedia($descritpion)
    {
        $this->descMedia = $descritpion;
    }

    // $MediaSteps GETTER
    public function getDescMedia()
    {
        return $this->descMedia;
    }

    // $MediaSteps SETTER
    public function setUrlMedia($url)
    {
        $this->urlMedia = $url;
    }

    // $MediaSteps GETTER
    public function getUrlMedia()
    {
        return $this->urlMedia;
    }

    // $MediaSteps SETTER
    public function setMediaSteps($arraySteps)
    {
        $this->arraySteps = $arraySteps;
    }

    // $MediaSteps GETTER
    public function getMediaSteps()
    {
        return $this->arraySteps;
    }

    // $MediaArticles SETTER
    public function setMediaArticles($arrayArticles)
    {
        $this->arrayArticles = $arrayArticles;
    }

    // $MediaArticles GETTER
    public function getMediaArticles()
    {
        return $this->arrayArticles;
    }

    // $dateCreaMedia SETTER
    public function setDateCreaMedia($dateCreaMedia)
    {
        $this->dateCreaMedia = $dateCreaMedia;
    }

    // $dateCreaMedia GETTER
    public function getDateCreaMedia()
    {
        return $this->dateCreaMedia;
    }

    // $dateUpdateMedia SETTER
    public function setDateUpdateMedia($dateUpdateMedia)
    {
        $this->dateUpdateMedia = $dateUpdateMedia;
    }

    // $dateUpdateMedia GETTER
    public function getDateUpdateMedia()
    {
        return $this->dateUpdateMedia;
    }

    // $statutMedia SETTER
    public function setStatutMedia($statutMedia)
    {
        $this->statutMedia = $statutMedia;
    }

    // $statutMedia GETTER
    public function getStatutMedia()
    {
        return $this->statutMedia;
    }

    // $statutMedia SETTER
    public function setOrderMedia($orderMedia)
    {
        $this->orderMedia = $orderMedia;
    }

    // $statutMedia GETTER
    public function getOrderMedia()
    {
        return $this->orderMedia;
    }

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------- METHODS -------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // hydratation of Media from datas
    public function hydrate($datas)
    {
        if(!$datas == "") {
            // We loop all datas (sdatas = array of all attributs of object)
            foreach ($datas as $key => $value) {
                // We create method to call setters with prefix 'set'
                $method = 'set' . ucfirst($key);

                // We check id the method exist
                if (method_exists($this, $method)) {
                    // We call the setter and inject the value
                    $this->$method($value);
                }
            }
        }
    }
}
