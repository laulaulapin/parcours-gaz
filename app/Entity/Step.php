<?php

namespace Entity;


class Step  {

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- ATTRIBUTS -------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    private $idStep;                //INT
    private $titleStep;             //VARCHAR(255)
    private $descStep;              //TEXT
    private $pictoStep;             //TEXT
    private $orderStep;             //INT
    private $arrayArticles;         // ARRAY OF ARTICLES table step_has_articles
    private $arrayMedias;         // ARRAY OF ARTICLES table step_has_articles
    private $dateCreaStep;          //DATETIME
    private $dateUpdateStep;        //DATETIME


    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ---------------------------------------------- SETTERS & GETTERS --------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // $idStep SETTER
    public function setIdStep($id)
    {
        if(intval($id))
        {
            $this->idStep = $id;
        }
    }

    // $idStep GETTER
    public function getIdStep()
    {
        return $this->idStep;
    }

    // $titleStep SETTER
    public function setTitleStep($title)
    {
        if(is_string($title))
        {
            $this->titleStep = $title;
        }
    }

    // $titleStep GETTER
    public function getTitleStep()
    {
        return $this->titleStep;
    }

    // $descStep SETTER
    public function setDescStep($description)
    {
        if(is_string($description))
        {
            $this->descStep = $description;
        }
    }

    // $descStep GETTER
    public function getDescStep()
    {
        return $this->descStep;
    }

    // $pictoStep SETTER
    public function setPictoStep($url)
    {
        if(is_string($url))
        {
            $this->pictoStep = $url;
        }
    }

    // $pictoStep GETTER
    public function getPictoStep()
    {
        return $this->pictoStep;
    }

    // $orderStep SETTER
    public function setOrderStep($order)
    {
        if(intval($order))
        {
            $this->orderStep = $order;
        }
    }

    // $orderStep GETTER
    public function getOrderStep()
    {
        return $this->orderStep;
    }

    // $ArticleSteps SETTER
    public function setStepArticles($arrayArticles)
    {
        $this->arrayArticles = $arrayArticles;
    }

    // $ArticleSteps GETTER
    public function getStepArticles()
    {
        return $this->arrayArticles;
    }

    // $ArticleSteps SETTER
    public function setStepMedias($arrayMedias)
    {
        $this->arrayMedias = $arrayMedias;
    }

    // $ArticleSteps GETTER
    public function getStepMedias()
    {
        return $this->arrayMedias;
    }

    // $dateCreaStep SETTER
    public function setDateCreaStep($date)
    {
        $this->dateCreaStep = $date;
    }

    // $dateCreaStep GETTER
    public function getDateCreaStep()
    {
        return $this->dateCreaStep;
    }

    // $dateUpdateStep SETTER
    public function setDateUpdateStep($date)
    {
        $this->dateUpdateStep = $date;
    }

    // $dateUpdateStep GETTER
    public function getDateUpdateStep()
    {
        return $this->dateUpdateStep;
    }

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* --------------------------------------------------- METHODS -------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // hydratation of Step from datas
    public function hydrate($datas)
    {
        if(!$datas == "") {
            // We loop all datas (sdatas = array of all attributs of object)
            foreach ($datas as $key => $value) {
                // We create method to call setters with prefix 'set'
                $method = 'set' . ucfirst($key);

                // We check id the method exist
                if (method_exists($this, $method)) {
                    // We call the setter and inject the value
                    $this->$method($value);
                }
            }
        }
    }
}
