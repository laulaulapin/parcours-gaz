<?php

//define routes
$routes = [
    /***************    ADMIN ROUTES     ***************/
    //  URL,                    METHOD NAME,        CONTROLLER
    // admin
    ["/login",                  "login",            "LoginController"],
    ["/admin",                  "showAdmin",        "AdminController"],
    ["/register",               "register",         "UserController"],
    ["/logout",                 "logout",           "LoginController"],


    // Main
	["/general",                        "showMain",         "MainController"],
    ["/general/ajouter",                "addMain",          "MainController"],
    ["/general/mise-a-jour",            "updateMain",       "MainController"],
    ["/general/supprimer",              "deleteMain",       "MainController"],
    ["/general/afficher-general",       "getMainForJson",   "MainController"],

    // Steps
    ["/etapes",                 "showSteps",            "StepController"],
    ["/etapes/ajouter",         "addStep",              "StepController"],
    ["/etapes/mise-a-jour",     "updateStep",           "StepController"],
    ["/etapes/supprimer",       "deleteStep",           "StepController"],
    ["/etapes/afficher",        "getAllStepsForJson",   "StepController"],
    ["/etapes/ordre",           "arrayOrder",           "StepController"],
    ["/etape",                  "showSimpleStep",       "StepController"],

    //Articles
    ["/gestion-des-articles",                 "showArticles",              "ArticleController"],
    ["/gestion-des-articles/ajouter",         "addArticle",                "ArticleController"],
    ["/gestion-des-articles/mise-a-jour",     "updateArticle",             "ArticleController"],
    ["/gestion-des-articles/supprimer",       "deleteArticle",             "ArticleController"],
    ["/gestion-des-articles/afficher",        "getAllArticlesForJson",     "ArticleController"],

    // Medias
    ["/medias",                 "showMedias",       "MediaController"],
    ["/medias/ajouter",         "addMedia",         "MediaController"],
    ["/medias/mise-a-jour",     "updateMedia",      "MediaController"],
    ["/medias/supprimer",       "deleteMedia",      "MediaController"],

    // Profils
    ["/utilisateurs",               "showUsers",        "UserController"],
    ["/utilisateurs/ajouter",       "addUser",          "UserController"],
    ["/utilisateurs/mise-a-jour",   "updateUser",       "UserController"],
    ["/utilisateurs/supprimer",     "deleteUser",       "UserController"],
    ["/utilisateurs/afficher",      "getUsersForJson",  "UserController"],



    /***************    FRONT ROUTES     ***************/
    // HomePage
    ["/parcours-gaz",                              "showFrontHome",         "FrontHomeController"],






	["/faq", "faq", "DefaultController"],
	["/legal", "legal", "DefaultController"],
];