<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 16/02/2017
 * Time: 15:34
 */

namespace Controller;

use Grill\Controller\BaseController;
use Manager\FileManager;
use Manager\MainManager;
use Entity\Main;
use Entity\File;


class MainController extends BaseController
{



    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- SHOW ------------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // Show list of Main details
    // Route = /general
    public function showMain()
    {
        $mainManager = new MainManager();
        $main = $mainManager->selectAllMain();
        $this->show("main/list", ["main" => $main], 'admin');
    }

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- ADD  ------------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    //Show form to add Main details
    //Route = /general/ajouter
    public function addMain()
    {

        if (isset($_POST['CreateMain'])) {
            //The Update Date is NOW
            $dateUpdateStep = new \DateTime();
            $dateUpdateStep = $dateUpdateStep->format("Y-m-d H:i:s");
            $_POST['DateUpdateMain'] = $dateUpdateStep;

            // secure variables
            $_POST['TitleMain'] = strip_tags($_POST['TitleMain']);
            $_POST['DescMain'] = strip_tags($_POST['DescMain']);


            //VERIFY if there is a new file
            if (!empty($_FILES['PictoMain']['name'])) {
                // UPLOAD the new FILE AND RETURN THE PATH (= urlmedia)
                $file = new File($_FILES['PictoMain']);
                $fileManager = new FileManager();
                $urlPicto = $fileManager->uploadMedia($file);
            } else {
                // hydrate the url of picto to update the new image
                $urlPicto = "";
            }

            // create new Main Object
            $main = new Main();

            // hydrate the url of picto to update the new image
            $main->setPictoMain($urlPicto);

            // hydrate all main execpt picto
            $main->hydrate($_POST);

            // We create an instance of UserManager to manipulate datas
            $mainManager = new MainManager();


            // Update user in db
            $mainManager->addMainManager($main);

            // array returned in ajax request
            $dataJson = json_encode($main->jsonSerialize());
            echo $dataJson;
            return $dataJson;

        } else {
            echo 'error';
            return false;
        }
    }


    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- UPDATE  ---------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    //Show form to update Main details
    //Route: /general/mise-a-jour
    public function updateMain()
    {
        if (isset($_POST['UpdateMain'])) {
            //The Update Date is NOW
            $dateUpdateStep = new \DateTime();
            $dateUpdateStep = $dateUpdateStep->format("Y-m-d H:i:s");
            $_POST['DateUpdateMain'] = $dateUpdateStep;

            // secure variables
            $_POST['TitleMain'] = strip_tags($_POST['TitleMain']);
            $_POST['DescMain'] = strip_tags($_POST['DescMain']);

            // validator variables
            $validator = new \Grill\Model\Validation\Validator();
            //length ok ?
            $validator->validateLengthBetween($_POST['TitleMain'], "TitleMain", "Le titre dois être compris entre 2 and 50 caractères", 2, 50);

            // create new Main Object
            $main = new Main();
            $main->hydrateSQL($_POST);

            //We research oldMain to DELETE old picto
            $oldMainManager = new MainManager();
            $oldMain = $oldMainManager->selectAllMain();
            $oldPictoMain = $oldMain->getPictoMain();

            //VERIFY if there is a new file
            if (!empty($_FILES['PictoMain']['name'])) {
                // DELETE the old pictogramme
                $oldPictoManager = new FileManager();
                $oldPictoManager->deleteMedia($oldPictoMain);

                // UPLOAD the new FILE AND RETURN THE PATH (= urlmedia)
                $file = new File($_FILES['PictoMain']);
                $fileManager = new FileManager();
                $urlPicto = $fileManager->uploadMedia($file);

                // hydrate the url of picto to update the new image
                $main->setPictoMain($urlPicto);

            } else {
                $urlPicto = $oldPictoMain;

                //hydrate the url of picto to update the new image
                $main->setPictoMain($urlPicto);
            }

            // We create an instance of UserManager to manipulate datas
            $mainManager = new MainManager();

            // Update user in db
            $mainManager->updateMainManager($main);


            // array returned in ajax request
            $dataJson = json_encode($main->jsonSerialize());
            echo $dataJson;
            return $dataJson;

        } else {
            return false;
        }

    }


    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- DELETE ----------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    //Show form to delete Main details
    // Route = /general/supprimer
    public function deleteMain()
    {
        if (isset($_POST['DeleteMain'])) {
            if (isset($_POST['IdMain']) && $_POST['IdMain'] != null) {
                $idMain = $_POST['IdMain'];
                $mainManager = new MainManager();
                $idExiste = $mainManager->selectAllMain()->getIdMain();
                if ($idExiste === $idMain) {
                    $pictoExiste = $mainManager->selectAllMain()->getPictoMain();
                    if ($pictoExiste) {
                        $fileManager = new FileManager();
                        $fileManager->deleteMedia($pictoExiste);
                    }
                    $delete = $mainManager->deleteMainManager($idMain);
                    if (isset($delete)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            return false;
        }
    }

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- DATA JSON -------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // function to return array of main to use in jquery
    //Route = /general/afficher-general
    public function getMainForJson()
    {
        // We create an instance of UserManager to manipulate datas
        $mainManager = new MainManager();
        $main = $mainManager->selectAllMain();

        if ($main === false) {
            //$dataJson = json_encode("empty");
            echo "empty";
            return false;
        } else {
            // array returned in ajax request
            $dataJson = json_encode($main->jsonSerialize());
            echo $dataJson;
            return $dataJson;
        }
    }


}