<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 16/02/2017
 * Time: 15:34
 */

namespace Controller;

use Entity\Article;
use Grill\Controller\BaseController;
use Manager\ArticleManager;


class ArticleController extends BaseController
{

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- SHOW ------------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    //Show list of Articles
    // Route = /gestion-des-articles
    public function showArticles()
    {
        $this->show("article/list", [], 'admin');
    }


    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- ADD  ------------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // Show form to add Articles details
    // Route = /gestion-des-articles/ajouter
    public function addArticle()
    {
        // we check if it is a creation or not with input 'CreateArticle'
        if (isset($_POST['CreateArticle'])) {
            //The Update Date is NOW
            $dateUpdateArticle = new \DateTime();
            $dateUpdateArticle = $dateUpdateArticle->format("Y-m-d H:i:s");
            $_POST['DateUpdateArticle'] = $dateUpdateArticle;
            $_POST['DateCreaArticle'] = $dateUpdateArticle;

            // secure variables
            $_POST['TitleArticle'] = strip_tags($_POST['TitleArticle']);
            $_POST['ContentArticle'] = strip_tags($_POST['ContentArticle']);

            // If $_POST['StatutArticle'] exist
            if (!isset($_POST['StatutArticle'])) {
                $_POST['StatutArticle'] = 0;
            }

            //after insertion of article we get the lastId to associate article to steps
            // If $_POST['ArticleSteps'] exist we add as far as line that associations
            if (!isset($_POST['ArticleSteps'])) {
                $_POST['ArticleSteps'] = 0;
            }

            // create new Article Object
            $article = new Article();

            // hydrate all article execpt picto
            $article->hydrate($_POST);

            // We create an instance of UserManager to manipulate datas
            $articleManager = new ArticleManager();
            $test = $articleManager->addArticleManager($article);


            // array returned in ajax request
            $dataJson = json_encode($articleManager->jsonSerialize());
            echo $dataJson;
            return $dataJson;

        } else {
            echo 'error';
            return false;
        }
    }


    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- UPDATE  ---------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // Show form to update Article details
    //Route: /etapes/mise-a-jour
    public function updateArticle()
    {
        if (isset($_POST['UpdateArticle']) && isset($_POST['IdArticle'])) {
            // get old date of creation by id
            $oldArticleManager = new ArticleManager();
            $_POST['DateCreaArticle'] = $oldArticleManager->selectOneArticle($_POST['IdArticle'])->getDateCreaArticle();

            //The Update Date is NOW
            $dateUpdateArticle = new \DateTime();
            $_POST['DateUpdateArticle'] = $dateUpdateArticle->format("Y-m-d H:i:s");

            // secure variables
            $_POST['TitleArticle'] = strip_tags($_POST['TitleArticle']);
            $_POST['ContentArticle'] = strip_tags($_POST['ContentArticle']);

            // If $_POST['StatutArticle'] exist
            if (!isset($_POST['StatutArticle'])) {
                $_POST['StatutArticle'] = 0;
            }


            // create new Article Object
            $article = new Article();
            $article->hydrateSQL($_POST);

            // We create an instance of UserManager to manipulate datas
            $articleManager = new ArticleManager();

            // Update user in db
            $articleManager->updateArticleManager($article);

            // array returned in ajax request
            $dataJson = json_encode($articleManager->jsonSerialize());
            echo $dataJson;
            return $dataJson;

        } else {
            return false;
        }

    }


    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- DELETE ----------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // Show form to delete Article details
    //Route: /gestion-des-articles/supprimer
    public function deleteArticle()
    {
        if (isset($_POST['DeleteArticle'])) {
            if (isset($_POST['IdArticle']) && $_POST['IdArticle'] != null) {
                $articleManager = new ArticleManager();

                if (!empty($articleManager->selectOneArticle($_POST['IdArticle'])))
                {
                    $delete = $articleManager->deleteArticleManager($_POST['IdArticle']);
                    if (isset($delete)) {
                        echo $delete;
                        return true;
                    } else {
                        echo 'Erreur Delete';
                        return false;
                    }
                } else {
                    echo 'Erreur getIdArticle';
                    return false;
                }


            } else {
                echo 'Erreur undefined $_POST[\'IdArticle\']';
                return false;
            }
        } else {
            echo 'Erreur $_POST[\'DeleteArticle\']';
            return false;
        }
    }


    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- DATA JSON -------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // function to return array of Article to use in jquery
    // Route = /etapes/afficher
    public function getAllArticlesForJson()
    {
        // We create an instance of UserManager to manipulate datas
        $articleManager = new ArticleManager();
        $articles = $articleManager->selectAllArticles();
        if ($articles === false) {
            //$dataJson = json_encode("empty");
            echo "empty";
            return false;
        } else {
            // array returned in ajax request
            $dataJson = json_encode($articleManager->jsonSerialize());
            echo $dataJson;
            return $dataJson;
        }
    }
}

?>