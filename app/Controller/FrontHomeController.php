<?php

namespace Controller;

use Grill\Controller\BaseController;
use Manager\StepManager;

class FrontHomeController extends BaseController
{


    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- SHOW ------------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // Show home of application
    // Route = /parcours-gaz
    public function showFrontHome()
    {
        // get nm of steps to play slider
        $stepManager = new StepManager();
        $nbOfSteps = $stepManager->selectAllSteps();

        $this->show("front/home", ["nbOfSteps" => $nbOfSteps], 'frontHomePage');
    }

}