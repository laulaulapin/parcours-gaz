<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 16/02/2017
 * Time: 16:05
 */

namespace Controller;

use Grill\Controller\BaseController;

class MediaController extends BaseController
{



    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- SHOW ------------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    //Show list of Media details
    //
    public function showMedias()
    {
        $this->show("media/list", [], 'admin');
    }

    /**
     * Show simple Media by id
     */
    public function showSimple()
    {
        $this->show("media/simple", [], 'admin');
    }

    /**
     * Show form to add Media details
     */
    public function addMedia()
    {
        $this->show("media/add", [], 'admin');
    }

    /**
     * Show form to update Media details
     */
    public function updateMedia()
    {
        $this->show("media/update", [], 'admin');
    }

    /**
     * Show form to delete Media details
     */
    public function deleteMedia()
    {
        $this->show("media/delete", [], 'admin');
    }
}