<?php
/**
 * Created by PhpStorm.
 * User: Laura
 * Date: 16/02/2017
 * Time: 15:34
 */

namespace Controller;

use Grill\Controller\BaseController;
use Manager\FileManager;
use Manager\StepManager;
use Entity\Step;
use Entity\File;


class StepController extends BaseController
{

    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- SHOW ------------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // Show list of Main details
    // Route = /etapes
    public function showSteps()
    {
        $this->show("step/list", [], 'admin');
    }

    //Show form to add Main details
    //Route = /etape
    public function showSimpleStep()
    {
        // get nm of steps to play slider
        $stepManager = new StepManager();
        if (!empty($_GET['id'])) {
            // We create an instance of UserManager to get datas of step
            $stepManager = new StepManager();
            // array returned in ajax request, if there is an id parameter in URL, method return one only step
            $dataJson = json_encode($stepManager->jsonSerialize());
            echo $dataJson;
            return $dataJson;
        } else {
            $nbOfSteps = $stepManager->selectAllSteps();
            $this->show("front/home", ["nbOfSteps" => $nbOfSteps], 'frontHomePage');
        }
    }



    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- DATA JSON -------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // function to return array of Step to use in jquery
    // Route = /etapes/afficher
    public function getAllStepsForJson()
    {
        // We create an instance of UserManager to manipulate datas
        $stepManager = new StepManager();
        $steps = $stepManager->selectAllSteps();
        if ($steps === false) {
            //$dataJson = json_encode("empty");
            echo "empty";
            return false;
        } else {
            // array returned in ajax request
            $dataJson = json_encode($stepManager->jsonSerialize());
            echo $dataJson;
            return $dataJson;
        }
    }


    /* -------------------------------------------------------------------------------------------------------------------- */
    /* ------------------------------------------------- GET  ------------------------------------------------------------- */
    /* -------------------------------------------------------------------------------------------------------------------- */

    // know orders of step which are available or not
    // Route = /etapes/ordre
    public function arrayOrder($available = true)
    {
        $stepManager = new StepManager();
        $arrayOrderUnavailable = $stepManager->selectAllOrder();
        // we return available order if true or unavailable if false
        if ($available === true) {
            for ($i = 1; $i <= 6; $i++) {
                $availableNumbers[] = $i;
            }
            $arrayOrderAvailable = array_diff($availableNumbers, $arrayOrderUnavailable);
            if (isset($_GET['idStep'])) {
                $arrayOrderAvailable[] = intval($_GET['idStep']);
                array_multisort($arrayOrderAvailable, SORT_NUMERIC, SORT_ASC);
                $dataJson = json_encode($arrayOrderAvailable);
            } else {
                $dataJson = json_encode($arrayOrderAvailable);
            }
        } else
            if ($available === false) {
                $dataJson = json_encode($arrayOrderUnavailable);
            } else {
                $dataJson = json_encode("error");
            }

        // array returned in ajax request
        echo $dataJson;
        return $dataJson;
    }
}