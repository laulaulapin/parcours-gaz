/* -------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------- FUNCTIONS TO DISPLAY CONTENT OF ALL STEPS ------------------------------------ */
/* -------------------------------------------------------------------------------------------------------------------- */

// Display table content when document are loaded
displayContent();

// function to load/reload content of table since BDD
function displayContent() {
    $.ajax({
        type: 'GET',
        url: "<?= BASE_URL ?>/etapes/afficher",
        dataType: "json",
        cache: false
    }).done(function (datas) {
        $('#mainContainer').addClass('shown');
        // to add all slides about steps, we need to loop on datas get by ajax
        // currentClass is just to initialize load of slider on the first slide (when i === 0)
        let currentClass;
        for (i = 0; i < datas.length; i++) {
            if (i === 0) {
                currentClass = "current";
            } else {
                currentClass = "";
            }
            // at each turn we build an article (slide) with all datas
            $('#slider').append("<article class='slide " + currentClass + "' id='" + datas[i].id + "'>" +
                "<div class='imgBloc'>" +
                "<img src='<?= BASE_MEDIAS ?>/" + datas[i].pictostep + "' />" +
                "</div>" +
                "<div class='blocText'>" +
                "<p class='blocLink'><a rel='history' href='' id='" + datas[i].id + "' class='goSimpleStep glyphicon glyphicon-plus'></a></p>" +
                "<div class='textStep'>" +
                "<h2>" + datas[i].titlestep + "</h2>" +
                "<div class='text clearfix'>" +
                "<p>" + datas[i].descstep + "</p>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "</article>");
        }
        // if fail we check if the table is not empty or we display error in console
    }).fail(function (errorMessage) {
        if (errorMessage.responseText == "empty") {
            var message = "La table des données est vide";
            MessageAlert("danger", message);
            console.log("fail.displayContent.empty");
            console.log(errorMessage)
        } else {
            var message = "Erreur de chargement des données, merci de recharger la page. <br /> Si le problème persiste, contactez laura.duval@grdf.fr.";
            MessageAlert("danger", message);
            console.log("fail.displayContent.errorLoad");
            console.log(errorMessage)
        }
    });
}


/* -------------------------------------------------------------------------------------------------------------------- */
/* ------------------------------------- FUNCTIONS TO DISPLAY CONTENT OF ONE STEP ------------------------------------- */
/* -------------------------------------------------------------------------------------------------------------------- */

// function to load/reload content of table since BDD
function displayContentSimpleStep(idStep) {
    $.ajax({
        type: 'GET',
        url: "<?= BASE_URL ?>/etape?id=" + idStep,
        dataType: "json",
        cache: false
    }).done(function (data) {
        let contentArticles = "";
        for (i = 0; i < data[0].steparticles.length; i++) {
            if(data[0].steparticles[i].statutarticle == 1){
                contentArticles +=  "<article class='grid-item'> " +
                                        "<div class='textArticle'>" +
                                            " <h2> " + data[0].steparticles[i].titlearticle + " </h2> " +
                                            "<div class='text clearfix'>" +
                                                " <p> " + data[0].steparticles[i].contentarticle.substr(0, 250) + " [...] </p>" +
                                            " </div> " +
                                        " </div> " +
                                        "<p class='blocLink'><a rel='history' href='' data-id='" + data[0].steparticles[i].idarticle + "' class='goSimpleArticle glyphicon glyphicon-plus'></a></p>" +
                                    "</article>";
            }
        }
        $('#stepContainer').html('');
        $('#stepContainer').load("<?= BASE_SITE ?>/app/templates/front/simpleStep.php", function () {
            $('#stepPicto').attr("href", "<?= BASE_URL ?>/parcours-gaz");
            $('#stepPicto img').attr("src", "<?= BASE_MEDIAS ?>/" + data[0].pictostep);
            $('#stepTitle').text(data[0].titlestep);
            $('#stepDescription').text(data[0].descstep);
            $('#contentArticles').append(contentArticles);
        });
        $('#mainContainer').toggleClass('shown');
        $('#stepContainer').toggleClass('shown');
    }).fail(function (errorMessage) {
        if (errorMessage.responseText == "empty") {
            var message = "La table des données est vide";
            MessageAlert("danger", message);
            console.log("fail.displayContentSimpleStep.empty");
        } else {
            var message = "Erreur de chargement des données, merci de recharger la page. <br /> Si le problème persiste, contactez laura.duval@grdf.fr.";
            MessageAlert("danger", message);
            console.log("fail.displayContentSimpleStep.errorLoad");
            console.log(errorMessage)
        }
    });
}

// click on button to display one step
/**
 /*  HELP about "DELEGATION" for elements wicht are created by jQuery
 $('_parent_container_').on('click', $('_element_'), function (elem){ ... });
 to target elements
 elem.currentTarget = this = _parent_container_
 elem.target = _element_
 */
$('#slider').on('click', $('article'), function (elem) {
    if (elem.target.closest("article")) {
        let id = elem.target.closest("article").id;
        displayContentSimpleStep(id);
    }
});

$('#stepContainer').on('click', $('article #grid-item'), function (elem) {
    let target = elem.target;
    console.log(target.nodeType.parent());
});



// click on img to return on the slider
$('#stepContainer').on('click', $('a#stepPicto img'), function (elem) {
    if (elem.target.closest("a")) {
        let targetId = elem.target.closest("a").id;
        if (targetId == "stepPicto") {
            $('#stepContainer').toggleClass('shown');
            $('#mainContainer').toggleClass('shown');
        }
    }
});

/* -------------------------------------------------------------------------------------------------------------------- */
/* -------------------------------------------- FUNCTION TO PLAY HOME SLIDER ------------------------------------------ */
/* -------------------------------------------------------------------------------------------------------------------- */


// we need to
// ADD counter to know max 'next' and 'prev'
// KNOW how many child are slider ($nbOfSteps is a data get by the view)
let count = 0;
const nbSlide = '<?= count($nbOfSteps) ?>';
const nbMax = nbSlide - 1;

// ALL ACTIONS TO MOVE SLIDER
// click on previous button, slide to left
$('#prev').on('click', function (e) {
    e.preventDefault();
    moveSlideToLeftAndRight("prev")
});
// click on next button, slide to right
$('#next').on('click', function (e) {
    e.preventDefault();
    moveSlideToLeftAndRight("next")
});
// qwipe on previous button, slide to left
$('#mainContainer').on("swipeleft", function () {
    moveSlideToLeftAndRight("next");
});

$('#mainContainer').on("swiperight", function () {
    moveSlideToLeftAndRight("prev")
});

// initialize variables to use in function moveSlideToLeftAndRight
let current;
let notCurrent;
let slideWidth;
let slideMarginLeft;
let slideMarginRight;
let widthToMove;

// create function moveSlideToLeftAndRight to play slider about his direction, place, nm of slides ...
function moveSlideToLeftAndRight(direction) {
    current = $('.slide.current');
    notCurrent = $('.slide:not(.current)');
    widthToMove = 250;
    if (direction == "next") {
        if (count < nbMax) {
            count++;
            $('#slider').animate({left: "-=" + widthToMove}, {
                duration: 500,
                specialEasing: {left: "easeOutCubic"}
            });
            addCurrentClass(current, 'next');
        } else {
            $('#slider').animate({left: "-=50"}, {duration: 200}).animate({left: "+=50"}, {duration: 200});
        }
    } else if (direction == "prev") {
        if (count > 0) {
            count--;
            $('#slider').animate({left: "+=" + widthToMove}, {
                duration: 500,
                specialEasing: {left: "easeOutCubic"}
            });
            addCurrentClass(current, 'prev');
        } else {
            $('#slider').animate({left: "+=50"}, {duration: 200}).animate({left: "-=50"}, {duration: 200});
        }
    }
}

// Toggle class current to the slide current clic and an other class to old slide to play animation
function addCurrentClass(current, prevOrNext) {
    current.toggleClass('current');
    if (prevOrNext == 'next') {
        let next = current.next();
        next.toggleClass('current');
    } else if (prevOrNext == 'prev') {
        let prev = current.prev();
        prev.toggleClass('current');
    }
    // add class on old slide to play animation
    current.addClass('recently-current');
    setTimeout(function () {
        current.removeClass('recently-current');
    }, 1000);

}
