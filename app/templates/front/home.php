<section id="multiple-steps-container">
    <header id="title">
        <h1>
            <img id="logo" src="<?= BASE_URL ?>/img/logo.svg"
        </h1>
    </header>
    <section id="slider-line-left"></section>
    <section id="slider-background"></section>
    <section id="slider-line-right"></section>
    <section id="slider"></section>
</section>
<script type="text/javascript">
    $(document).on('ready', function () {
        <?php include('includes/LoadContentHome.js'); ?>
    });
</script>
