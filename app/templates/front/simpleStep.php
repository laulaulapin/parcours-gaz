<section id="simple-step-container">
    <aside id="stepChosen">
        <div id="imgBlocSimple">
            <a id="stepPicto" href=""><img src=""/></a>
        </div>
        <div id="blocTextSimple">
            <div id="textStepSimple">
                <h1 id="stepTitle"></h1>
                <div class="text">
                    <p id="stepDescription"></p>
                </div>
            </div>
        </div>
    </aside>
    <div id="filter-nav">
    <nav>
        <ul id="nav-content-top">
            <li id="tout" class="nav-content"><a href=""><span>TOUT</span></a></li>
            <li id="articles" class="nav-content"><a href=""><span>ARTICLES</span></a></li>
            <li id="medias" class="nav-content"><a href=""><span>MÉDIAS</span></a></li>
            <li id="videos" class="nav-content"><a href=""><span>VIDÉOS</span></a></li>
            <li id="images" class="nav-content"><a href=""><span>IMAGES</span></a></li>
            <li id="stages" class="nav-content"><a href=""><span>STAGES</span></a></li>
        </ul>
    </nav>
    <nav>
        <ul id="nav-content-bottom">
            <li id="accueil" class="nav-content"><a href=""><span>ACCUEIL</span></a></li>
        </ul>
    </nav>
    </div>
    <section id="contentArticles" class="grid">
    </section>
    <section id="contentSimpleArticle" class="grid">
    </section>
</section>
