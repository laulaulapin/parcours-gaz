<?php //la variable $grill_user est déclarée dans \Grill\View\View ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= (isset($title)) ? $title : "Administration, Parcours Gaz !"; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- STYLESHEETS -->
    <link href="<?= BASE_URL ?>/css/normalize.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/css/animate.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/css/globalFront.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/css/frontHome.css" rel="stylesheet">
    <link href="<?= BASE_URL ?>/css/frontSimple.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= BASE_URL ?>/css/jquery.mobile-1.0.min.css"/>

    <link rel="icon" href="<?= BASE_URL ?>/img/favicon.png">


    <!-- SCRIPTS -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>-->
    <script src="<?= BASE_URL ?>/js/jquery-1.12.4.min.js"></script>


    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= BASE_URL ?>/js/bootstrap.min.js"></script>
    <!-- Include library of js Mobile -->
    <script src="<?= BASE_URL ?>/js/jquery.mobile-1.4.5.min.js"></script>
    <script type="text/javascript" src="<?= BASE_URL ?>/js/jquery.easing.1.3.js"></script>

    <!-- Masonry -->
    <script src="<?= BASE_URL ?>/js/masonry.js"></script>

    <!-- Global Application script -->
    <script src="<?= BASE_URL ?>/js/global.js"></script>


</head>


<body data-role="page" oncontextmenu="return false" ondragstart="return false" >
<main role="main" class="ui-content containerHome">
    <aside id="prev">
        <a href="" class="glyphicon glyphicon-chevron-left"></a>
    </aside>
    <section id="articleContainer"></section>
    <section id="stepContainer"></section>
    <section id="mainContainer">

            <?php include($grill_page); ?>

    </section>
    <aside id="next">
        <a href="" class="glyphicon glyphicon-chevron-right"></a>
    </aside>
</main>


</body>

</html>