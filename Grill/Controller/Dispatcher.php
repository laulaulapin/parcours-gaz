<?php
namespace Grill\Controller;

class Dispatcher
{
	/**
	 * Tente de trouver l'url dans la liste des routes. Appelle la méthode associée.
	 */
	public function match($routes, $url)
	{
		//on cherche l'URL actuelle dans notre tableau de routes
		foreach($routes as $route){
			if ($route[0] == $url){
				//récupère la méthode associée à l'URL
				$methodName = $route[1];	
				//recrée le nom complet du contrôleur à instancier
				$controllerName = "Controller\\" . $route[2]; 
				//instancie le contrôleur
				$controller = new $controllerName();
				//appelle la méthode
				return $controller->$methodName();
			}
		}

		//404
		$controller = new BaseController();
		$controller->notFound();
	}

}