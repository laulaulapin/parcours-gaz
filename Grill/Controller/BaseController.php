<?php

namespace Grill\Controller;

/**
 * Contrôleur de base à extender par les contrôleurs de l'appli
 */
class BaseController
{
	/**
	 * Affiche le template (raccourci)
	 */
	public function show($filename, $data = [], $templateType)
	{
		$view = new \Grill\View\View();
		$view->showTemplate($filename, $data, $templateType);
		die();
	}


	/**
	 * Erreur 404
	 */
	public function notFound()
	{
		header("HTTP/1.0 404 Not Found");
		echo "Oupssss 404!";
		die();
	}

	/**
	 * Erreur 403
	 */
	public function forbidden()
	{
		header('HTTP/1.0 403 Forbidden');
		echo "Nope. 403.";
		die();
	}

}