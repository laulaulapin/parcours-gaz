<?php

namespace Grill\View;

class View
{

	//nom de variables chelou pour éviter les collisions causées par le extract()
	public function showTemplate($grill_filename, $grill_data = [], $grill_template)
	{
		//crée une variable nommée comme chacune des clefs du tableau
		//la valeur reste la valeur pour chacune des variables
		extract($grill_data);

		$grill_page = "../app/templates/$grill_filename.php";

        if ($grill_template === "frontHomePage")
        {
            include("../app/templates/frontHomePage.php");
        } else if ($grill_template === "frontSimpleStep")
        {
            include("../app/templates/frontSimpleStep.php");
        } else { return false; }



	}

}